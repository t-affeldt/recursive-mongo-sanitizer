# recursive-mongo-sanitizer

A sanitizer for MongoDB to be used with Node.
The sanitize function will remove any object properties that could be used as a MongoDB command.
As such, all keys starting with a dollar sign (like $ne or $gt) will be removed from the returned object.

Install this package by running `npm install recursive-mongo-sanitizer`.

## Motivation
This module came to be in order to fix two design flaws with the widely used [mongo-sanitize](https://www.npmjs.com/package/mongo-sanitize) library:

1. mongo-sanitize only does a shallow scan for malicious property names. Thus, the property starting with a dollar sign will not be removed when using e.g. `sanitize({x:{$ne:null}})`.
2. The library will not only return a sanitized version of the passed object; it will mutate the original variable as well, possibly leading to confusion.

This module can be used the same way as mongo-sanitize, but fixes the aforementioned issues. The provided function will perform a deep scan for potentially malicious property names and return a sanitized copy.

## Usage
```javascript
const sanitize = require('recursive-mongo-sanitizer');

const cleanParams = sanitize(req.params);
Users.findOne({name: cleanParams.name}, (err, doc) => {
	// ...
});
```