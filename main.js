"use strict";

function sanitize(input) {
	if (!(input instanceof Object)) {
		return input;
	}

	const sanitizedEntries = Object.entries(input)
		.filter(([key, value]) => !key.startsWith('$'))
		.map(([key, value]) => [key, sanitize(value)]);
	return Object.fromEntries(sanitizedEntries);
}

module.exports = sanitize;
